using System;
using System.IO;

var path = Args[0];
try{
		using (StreamReader reader = new StreamReader(path)){
				string line;
				while ((line = reader.ReadLine()) != null){
					var values = line.Split(',');
					if ( values[int.Parse(Args[1])] == Args[2]){
						Console.WriteLine(values[0] + ", " + values[1] + ", " + values[2] + ", " + values[3]);
					}
				}
		}
	}
catch (Exception e)
	{
		Console.WriteLine(e.Message);
	}
Console.ReadKey();

